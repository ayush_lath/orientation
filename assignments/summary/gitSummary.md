# Git Summary
**Git** is a type of version control system (VCS) that makes it easier to track changes to files.

![Alt](https://www.nobledesktop.com/image/blog/git-branches-merge.png)

**Repository** - collection of stored code i.e files and folders.
**Commit** - point in time snapshot of repository with changes.
**Push** - syncing your commits.
**Branch** - pointer to the head of group of commits.
**Merge** - integrating the branch with main code.
**Clone** - making a copy of main code into local machine. 
**Fork** - copy repository with your username.


## Git Internals
**Modified** -  changed the file but not committed.
**Staged**  -  marked the changes but not committed yet.
**Committed** -  that changes have been saved to repository
**Workspace** -  the tree of repo in which you make all the changes through Editor(s). 
**Staging** -  the repo in which all the staged files are saved. 
**Local Repository** -  path where all the committed files are saved. 
**Remote Repository** -  the copy of local repo on some server, changes made on loacl repo doesn't affect this repo.

 ## Git Workflow
- **Clone repo** -
  - $ git clone (link to repository)
- **Create new branch** - 
  - $ git checkout master
  - $ git checkout -b (your branch name)
- **Staging changes** -
  - $ git add .  # To add untracked files
- **Commit changes** -
  - $ git commit -sv # Description about the commit
- **Push Files** -
  - $ git push origin (branch name)  # push changes into repository

![Alt Text](https://i.redd.it/nm1w0gnf2zh11.png)
